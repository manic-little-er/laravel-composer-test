<?php

namespace Yongm\LaravelComposerTest\Providers;

use Illuminate\Support\ServiceProvider;

class TestProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        echo "boot";
    }
}